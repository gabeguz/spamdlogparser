package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"time"
	//"github.com/influxdata/influxdb/client/v2"
)

//var logLine = "Dec  7 14:50:22 redbox spamd[13773]: 197.14.14.150: connected (1/0)"
//var logLine = "Dec  7 14:50:29 redbox spamd[13773]: 197.14.14.150: disconnected after 7 seconds."

const logTimeFormat = "Jan 2 15:04:05 MST 2006"

type spamHost struct {
	ip            net.IP
	firstSeen     time.Time
	lastSeen      time.Time
	timesSeen     int
	secondsWasted int
}

var hosts = map[string]spamHost{}

func usage() {
	fmt.Fprintf(os.Stderr, "usage: spamdlogparser [path]\n")
	flag.PrintDefaults()
	os.Exit(2)
}

func parse(logLine string) error {
	var s spamHost
	// first 15 chars of the line are the date/time
	timeString := logLine[0:15]
	t, err := time.Parse(logTimeFormat, timeString+" EST 2016")

	if err != nil {
		fmt.Errorf("could not parse time: %s", timeString)
	}

	if s.firstSeen.IsZero() {
		s.firstSeen = t
	}

	logSections := strings.Split(logLine, ":")
	// a minimum parseable log line has 5 ":" separated sections:
	// date hour:minute:second host daemon[pid]:ip:message
	if len(logSections) < 5 {
		return fmt.Errorf("could not parse log line: %s", logLine)
	}

	s.ip = net.ParseIP(strings.TrimSpace(logSections[3]))
	if s.ip == nil {
		fmt.Errorf("could not parse ip: %v\n", logSections[3])
	}

	msg := strings.TrimSpace(logSections[4])
	// if message starts with "connected" increment the 'timesSeen' counter
	if strings.HasPrefix(msg, "connected") {
		s.timesSeen++
		// if message starts with "disconnected" add the number of seconds to
		// the 'secondsWasted' counter
	} else if strings.HasPrefix(msg, "disconnected after") {
		msgSections := strings.Split(msg, " ")
		seconds, err := strconv.Atoi(msgSections[2])
		if err != nil {
			fmt.Errorf("can't convert %s to int", msgSections[2])
		} else {
			s.secondsWasted += seconds
		}
	}

	// Check for host in map, and merge if already exists
	host, ok := hosts[string(s.ip.To16())]
	if ok {
		host.timesSeen++
		host.secondsWasted += s.secondsWasted
		host.ip = s.ip
		hosts[string(s.ip.To16())] = host
	} else {
		hosts[string(s.ip.To16())] = s
	}

	fmt.Printf("IP: %v\n", s.ip)

	return nil
}

func main() {
	// read a spamd log file, and parse it for insertion into a db, or rrdtool
	// log lines look like this:
	// Dec  5 21:00:01 redbox newsyslog[41663]: logfile turned over
	// Dec  7 14:50:22 redbox spamd[13773]: 197.14.14.150: connected (1/0)
	// Dec  7 14:50:29 redbox spamd[13773]: 197.14.14.150: disconnected after 7 seconds.

	// time, ip, active connections, number of connections
	// how long it was connected
	flag.Parse()
	args := flag.Args()
	if len(args) < 1 {
		usage()
	}

	f, err := os.Open(args[0])
	if err != nil {
		log.Fatal(err)
	}

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		err := parse(scanner.Text())
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
	}

	for _, v := range hosts {
		fmt.Printf("Host: %v Connected %d times and wasted %d seconds.\n", v.ip.String(),
			v.timesSeen, v.secondsWasted)
	}

}

// Read from standard in:
// scanner := bufio.NewScanner(os.Stdin)
// for scanner.Scan() {
//		fmt.Println(scanner.Text()) // Println will add back the final '\n'
//	}
//	if err := scanner.Err(); err != nil {
//		fmt.Fprintln(os.Stderr, "reading standard input:", err)
//	}
